"use strict";

// Setup the Form.IO server.
const express = require("express");
const cors = require("cors");
const router = express.Router();
const multer = require("multer");
const GridFsStorage = require("multer-gridfs-storage");
const myCrypto = require("crypto");
var path = require("path");
const mongoose = require("mongoose");
mongoose.Promise = global.Promise;
var app = express();
/////porta

const mongoosePortal = require("mongoose");
mongoosePortal.Promise = global.Promise;

//////////////////
const bodyParser = require("body-parser");
const methodOverride = require("method-override");
const _ = require("lodash");
const events = require("events");
const Q = require("q");
const nunjucks = require("nunjucks");
const util = require("./src/util/util");
const log = require("debug")("formio:log");
var dateTime = require("node-datetime");
// Keep track of the formio interface.
router.formio = {};

/////portal
router.portal = {};
router.portal.mongoose = mongoosePortal;

// Allow libraries to use a single instance of mongoose.
router.formio.mongoose = mongoose;

// Use custom template delimiters.
_.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

// Allow custom configurations passed to the Form.IO server.
module.exports = function(config) {
  // Give app a reference to our config.
  router.formio.config = config;

  // Add the middleware.
  router.formio.middleware = require("./src/middleware/middleware")(router);

  // Configure nunjucks to not watch any files
  nunjucks.configure([], {
    watch: false
  });

  // Allow events to be triggered.
  router.formio.events = new events.EventEmitter();
  router.formio.config.schema = require("./package.json").schema;

  router.formio.log = (event, req, ...info) => {
    const result = router.formio.hook.alter("log", event, req, ...info);

    if (result) {
      log(event, ...info);
    }
  };

  /**
   * Initialize the formio server.
   */
  router.init = function(hooks) {
    const deferred = Q.defer();

    // Hooks system during boot.
    router.formio.hooks = hooks;

    // Add the utils to the formio object.
    router.formio.util = util;

    // Get the hook system.
    router.formio.hook = require("./src/util/hook")(router.formio);

    // Get the encryption system.
    router.formio.encrypt = require("./src/util/encrypt");

    // Load the updates and attach them to the router.
    router.formio.update = require("./src/db/index")(router.formio);

    // Run the healthCheck sanity check on /health
    /* eslint-disable max-statements */
    router.formio.update.initialize(function(err, db) {
      // If an error occurred, then reject the initialization.
      if (err) {
        return deferred.reject(err);
      }

      util.log("Initializing API Server.");

      // Add the database connection to the router.
      router.formio.db = db;

      // Establish our url alias middleware.
      if (!router.formio.hook.invoke("init", "alias", router.formio)) {
        router.use(router.formio.middleware.alias);
      }

      // Establish the parameters.
      if (!router.formio.hook.invoke("init", "params", router.formio)) {
        router.use(router.formio.middleware.params);
      }

      // Add the db schema sanity check to each request.
      router.use(router.formio.update.sanityCheck);

      // Add Middleware necessary for REST API's
      router.use(bodyParser.urlencoded({ extended: true }));
      router.use(
        bodyParser.json({
          limit: "16mb"
        })
      );
      router.use(methodOverride("X-HTTP-Method-Override"));

      // Error handler for malformed JSON
      router.use(function(err, req, res, next) {
        if (err instanceof SyntaxError) {
          res.status(400).send(err.message);
        }

        next();
      });

      // CORS Support
      const corsRoute = cors(router.formio.hook.alter("cors"));
      router.use(function(req, res, next) {
        if (req.url === "/") {
          return next();
        }

        if (res.headersSent) {
          return next();
        }

        corsRoute(req, res, next);
      });

      // Import our authentication models.
      router.formio.auth = require("./src/authentication/index")(router);

      // Perform token mutation before all requests.
      if (!router.formio.hook.invoke("init", "token", router.formio)) {
        router.use(router.formio.middleware.tokenHandler);
      }

      // The get token handler
      if (!router.formio.hook.invoke("init", "getTempToken", router.formio)) {
        router.get("/token", router.formio.auth.tempToken);
      }

      // The current user handler.
      if (!router.formio.hook.invoke("init", "logout", router.formio)) {
        router.get("/logout", router.formio.auth.logout);
      }

      // The current user handler.
      if (!router.formio.hook.invoke("init", "current", router.formio)) {
        router.get(
          "/current",
          router.formio.hook.alter("currentUser", [
            router.formio.auth.currentUser
          ])
        );
      }

      // The access handler.
      if (!router.formio.hook.invoke("init", "access", router.formio)) {
        router.get("/access", router.formio.middleware.accessHandler);
      }

      // Authorize all urls based on roles and permissions.
      if (!router.formio.hook.invoke("init", "perms", router.formio)) {
        // router.use(router.formio.middleware.permissionHandler);
      }

      ///////portal

      let mongoPortalUrl = config.mongoPortal;
      const mongoPortalConfig = config.mongoConfig
        ? JSON.parse(config.mongoConfig)
        : {};
      if (!mongoPortalConfig.hasOwnProperty("connectTimeoutMS")) {
        mongoPortalConfig.connectTimeoutMS = 300000;
      }
      if (!mongoPortalConfig.hasOwnProperty("socketTimeoutMS")) {
        mongoPortalConfig.socketTimeoutMS = 300000;
      }
      if (!mongoPortalConfig.hasOwnProperty("useNewUrlParser")) {
        mongoPortalConfig.useNewUrlParser = true;
      }
      if (!mongoPortalConfig.hasOwnProperty("keepAlive")) {
        mongoPortalConfig.keepAlive = 120;
      }
      if (process.env.MONGO_HIGH_AVAILABILITY) {
        mongoPortalConfig.mongos = true;
      }
      if (_.isArray(config.mongo)) {
        mongoPortalUrl = config.mongo.join(",");
        mongoPortalConfig.mongos = true;
      }
      if (config.mongoSA) {
        mongoPortalConfig.sslValidate = true;
        mongoPortalConfig.sslCA = config.mongoSA;
      }

      // Connect to MongoDB.
      mongoosePortal.connect(mongoPortalUrl, mongoPortalConfig);

      // Trigger when the connection is made.
      mongoosePortal.connection.on("error", function(err) {
        util.log(err.message);
        deferred.reject(err.message);
      });

      mongoosePortal.connection.once("open", function() {
        util.log(" > Mongo portal connection established.");

        router.portal.BaseModel = require("./portalSrc/models/BaseModel");

        // Load the plugins.
        router.portal.plugins = require("./portalSrc/plugins/plugins");

        // Get the models for our project.
        const portalModels = require("./portalSrc/models/models")(router);

        router.portal.resources = require("./portalSrc/resources/resources")(
          router
        );
        // Load the Schemas.
        router.formio.schemas = _.assign(
          router.portal.schemas,
          portalModels.schemas
        );

        // Load the Models.
        router.portal.models = portalModels.models;

        router.get("/getUsers", function(req, res, next) {
          util.log("get users");
          router.portal.resources.user.model.find({}, function(err, users) {
            if (err) {
              return next(err);
            }

            if (!users) {
              return res.status(404).send("Form not found");
            }

            res.setHeader("Content-Type", "application/json");
            res.send({ data: users });
          });
        });

        router.post("/userInsert", function(req, res, next) {
          util.log("return new676 message222");

          router.portal.resources.user.model.find(
            { displayName: req.body.displayName },
            function(err, docs) {
              if (docs.length) {
                util.log("return jjj");
                res.send(docs, 200);
              } else {
                router.portal.resources.user.model.create(
                  { email: req.body.email, displayName: req.body.displayName },
                  function(err, item) {
                    if (err) {
                      util.log();
                    }
                    util.log("return jjj");

                    res.send({
                      cats: [{ name: "lilly" }, { name: "lucy" }]
                    });
                  }
                );
              }
            }
          );
        });

        router.post("/updateUserPass", function(req, res, next) {
          util.log("update user pass");

          var query = { _id: mongoose.Types.ObjectId(req.body._id) };
          var update = {
            $set: { pass: req.body.pass, needsAuthentication: true }
          };

          router.portal.resources.user.model.findOneAndUpdate(
            query,
            update,
            { new: true },
            function(err, item) {
              if (err) {
                // return done(err);
              }
            }
          );
        });

        router.post("/confirmPass", function(req, res, next) {
          util.log("confirmPass");

          var query = { _id: req.body._id };
          var update = { $set: { needsAuthentication: false } };

          router.portal.resources.user.model.findOneAndUpdate(
            query,
            update,
            function(err, item) {
              if (err) {
                // return done(err);
              }
            }
          );
        });

        router.post("/updateOpenedMessage", function(req, res, next) {
          util.log("return new676 message");

          var query = { _id: req.body._id };
          var update = { $set: { isMsgNew: false } };

          router.portal.resources.inbox.model.findOneAndUpdate(
            query,
            update,
            function(err, item) {
              if (err) {
                // return done(err);
              }
            }
          );
        });
        router.use(bodyParser.json());
        router.post("/updateMessageStatus", function(req, res, next) {
          util.log("update message status");

          var query = { _id: req.body.message.id };
          var update = {
            $set: {
              isDeleted: req.body.message.isDeleted,
              isImportant: req.body.message.isImportant,
              isStared: req.body.message.isStared,
              careProcessStatus: req.body.message.careProcessStatus
            }
          };

          router.portal.resources.inbox.model.findOneAndUpdate(
            query,
            update,
            function(err, item) {
              if (err) {
                // return done(err);
              }

              res.setHeader("Content-Type", "application/json");
              res.send({ data: {} });
              //done();
            }
          );
        });

        router.post("/inboxInsert", function(req, res, next) {
          util.log("inbox insert");

          if (req.body.inboxType == "reply") {
            var query = { _id: req.body.id };
            let statuses = req.body.statuses;
            statuses.push({
              status: "בטיפול עוס",
              date: dateTime.create(),
              color: "#f90"
            });
            var update = {
              $inc: { msgCount: 1 },
              $set: {
                content: req.body.content,
                isMsgNew: true,
                statuses: statuses,
                from: req.body.from,
                to: req.body.to[0].displayName
              }
            };

            router.portal.resources.inbox.model.findOneAndUpdate(
              query,
              update,
              function(err, item) {
                if (err) {
                  // return done(err);
                }

                res.setHeader("Content-Type", "application/json");
                res.send({ data: {} });
                //done();
              }
            );
          } else {
            /////////////////////
            let recipientsCount = req.body.to.length;
            let recipients = "";
            let hideInSent = recipientsCount > 1 ? true : undefined;
            req.body.to.forEach(function(aClient) {
              recipients += aClient.displayName + ",";
              let content = req.body.content.replace(
                "[שם לקוח]",
                aClient.displayName
              );

              sendMessage(
                aClient,
                req,
                content,
                hideInSent,
                aClient.displayName
              );
            });
            if (recipientsCount > 1) {
              recipients = recipients.replace(/,\s*$/, "");
              sendMessage(
                req.body.to[0],
                req,
                req.body.content,
                undefined,
                recipients
              );
            }
            res.setHeader("Content-Type", "application/json");
            res.send({ data: {} });
            /////////////////////////////
          }
        });

        function sendMessage(aClient, req, content, hideInSent, to) {
          router.portal.resources.inbox.model.create(
            {
              from: req.body.from,
              to: to,
              content: content,
              subject: req.body.subject,
              actions: ["action1, action2, action3"],
              data: {},
              msgCount: 0,
              isMsgNew: true,
              isImportant: req.body.isImportant,
              statuses: [
                { status: "חדשה", date: dateTime.create(), color: "#09f" }
              ],
              files: req.body.files,
              forms: req.body.forms,
              isForm: req.body.isForm,
              isDeleted: req.body.isDeleted,
              isMsgForMeeting: req.body.isMsgForMeeting,
              hideInSent: hideInSent
            },
            function(err, item) {
              if (err) {
                res.send(err);
                // return done(err);
              }

              // res.setHeader("Content-Type", "application/json");
              // res.send({ data: {} });
              //done();
            }
          );
        } // var storage = new GridFsStorage({
        //   //url: mongoose.connection.client.s.url,
        //   //options: options,
        //   db: mongoosePortal.connection,
        //   file: (req, file) => {
        //     return new Promise((resolve, reject) => {
        //       myCrypto.randomBytes(16, (err, buf) => {
        //         if (err) {
        //           return reject(err);
        //         }
        //         const filename =
        //           buf.toString("hex") + path.extname(file.originalname);
        //         const fileInfo = {
        //           filename: filename,
        //           bucketName: "uploads"
        //         };
        //         resolve(fileInfo);
        //       });
        //     });
        //   }
        // });

        var storage = multer.diskStorage({
          destination: function(req, file, cb) {
            cb(null, path.join(__dirname + "/uploads/"));
          },
          filename: function(req, file, cb) {
            cb(null, file.originalname);
          }

          // destination: function(req, file, cb) {
          //   cb(null, "uploads");
          // },
          // filename: function(req, file, cb) {
          //   //cb(null, file.fieldname + "-" + Date.now());
          //   cb(null, file.originalname);
          // }
        });

        const upload = multer({ storage });
        router.post("/inboxupload", upload.single("file"), fileUpload);

        module.exports = router;

        function fileUpload(req, res) {
          console.log("fileUpload");
          try {
            res.send({ file: req.file });
          } catch (err) {
            console.log(err);
            res.send(err);
          }
        }

        router.get("/fileDownload", fileDownload);

        module.exports = router;

        function fileDownload(req, res) {
          let fileName = req.query.fileName;
          console.log("fileDownload");
          try {
            res.sendFile(path.join(__dirname + "/uploads/" + fileName));
          } catch (err) {
            console.log(err);
            res.send(err);
          }
        }

        router.get("/completedForm2", function(req, res, next) {
          util.log("completed form");

          var query = { _id: req.query.message };
          router.portal.resources.inbox.model
            .findOne(query)
            .then(doc => {
              var item = doc.forms.find(o => o._id === req.query.form);
              item["submission"] = req.query.submission;

              doc.markModified("forms");

              doc.save();

              //sent respnse to client
            })
            .catch(err => {
              console.log("Oh! Dark");
            });

          res.send({
            cats: [{ name: "lilly" }, { name: "lucy" }]
          });
        });

        router.get("/getInbox", function(req, res, next) {
          util.log("get inbox");

          var filtera = {
            isDeleted: req.query.isDeleted
          };

          if (req.query.isForm == "true") filtera.isForm = req.query.isForm;

          if (req.query.isStared) filtera.isStared = req.query.isStared;

          if (req.query.isImportant)
            filtera.isImportant = req.query.isImportant;

          if (req.query.careProcessStatus)
            filtera.careProcessStatus = req.query.careProcessStatus;

          if (req.query.inboxType == "sent") {
            filtera.hideInSent = undefined;
            filtera.from = req.query.client;
          } else filtera.to = req.query.client;
          router.portal.resources.inbox.model
            .find(filtera, function(err, inbox) {
              if (err) {
                return next(err);
              }

              if (!inbox) {
                return res.status(404).send("Form not found");
              }

              //   res.setHeader("Content-Type", "application/json");
              res.send({ data: inbox });
            })
            .sort({ modified: "desc" });
        });

        router.get("/getUser", function(req, res, next) {
          util.log("get user");

          var filtera = {
            _id: req.query.id
          };

          router.portal.resources.user.model.find(filtera, function(err, user) {
            if (err) {
              return next(err);
            }

            if (!user) {
              return res.status(404).send("Form not found");
            }

            //   res.setHeader("Content-Type", "application/json");
            res.send({ data: user });
          });
        });

        // Load the Resources.

        // Return the form components.
        // deferred.resolve(router.portal);
      });

      //////////////////////////////

      let mongoUrl = config.mongo;
      const mongoConfig = config.mongoConfig
        ? JSON.parse(config.mongoConfig)
        : {};
      if (!mongoConfig.hasOwnProperty("connectTimeoutMS")) {
        mongoConfig.connectTimeoutMS = 300000;
      }
      if (!mongoConfig.hasOwnProperty("socketTimeoutMS")) {
        mongoConfig.socketTimeoutMS = 300000;
      }
      if (!mongoConfig.hasOwnProperty("useNewUrlParser")) {
        mongoConfig.useNewUrlParser = true;
      }
      if (!mongoConfig.hasOwnProperty("keepAlive")) {
        mongoConfig.keepAlive = 120;
      }
      if (process.env.MONGO_HIGH_AVAILABILITY) {
        mongoConfig.mongos = true;
      }
      if (_.isArray(config.mongo)) {
        mongoUrl = config.mongo.join(",");
        mongoConfig.mongos = true;
      }
      if (config.mongoSA) {
        mongoConfig.sslValidate = true;
        mongoConfig.sslCA = config.mongoSA;
      }

      // Connect to MongoDB.
      //  mongoose.connect(mongoUrl, mongoConfig);

      // Trigger when the connection is made.
      mongoose.connection.on("error", function(err) {
        util.log(err.message);
        deferred.reject(err.message);
      });

      // Called when the connection is made.
      mongoose.connection.once("open", function() {
        util.log(" > Mongo connection established.");

        // Load the BaseModel.
        router.formio.BaseModel = require("./src/models/BaseModel");

        // Load the plugins.
        router.formio.plugins = require("./src/plugins/plugins");

        router.formio.schemas = {
          PermissionSchema: require("./src/models/PermissionSchema")(
            router.formio
          ),
          AccessSchema: require("./src/models/AccessSchema")(router.formio)
        };

        // Get the models for our project.
        const models = require("./src/models/models")(router);

        // Load the Schemas.
        router.formio.schemas = _.assign(router.formio.schemas, models.schemas);

        // Load the Models.
        router.formio.models = models.models;

        // Load the Resources.
        router.formio.resources = require("./src/resources/resources")(router);

        // Load the request cache
        router.formio.cache = require("./src/cache/cache")(router);

        // Return the form components.
        router.get("/form/:formId/components", function(req, res, next) {
          router.formio.resources.form.model.findOne(
            { _id: req.params.formId },
            function(err, form) {
              if (err) {
                return next(err);
              }

              if (!form) {
                return res.status(404).send("Form not found");
              }
              // If query params present, filter components that match params
              const filter =
                Object.keys(req.query).length !== 0
                  ? _.omit(req.query, ["limit", "skip"])
                  : null;
              res.json(
                _(util.flattenComponents(form.components))
                  .filter(function(component) {
                    if (!filter) {
                      return true;
                    }
                    return _.reduce(
                      filter,
                      function(prev, value, prop) {
                        if (!value) {
                          return prev && _.has(component, prop);
                        }
                        const actualValue = _.property(prop)(component);
                        // loose equality so number values can match
                        return (
                          (prev && actualValue == value) || // eslint-disable-line eqeqeq
                          (value === "true" && actualValue === true) ||
                          (value === "false" && actualValue === false)
                        );
                      },
                      true
                    );
                  })
                  .values()
                  .value()
              );
            }
          );
        });

        // Import the form actions.
        router.formio.Action = router.formio.models.action;
        router.formio.actions = require("./src/actions/actions")(router);

        // Add submission data export capabilities.
        require("./src/export/export")(router);

        // Add the available templates.
        router.formio.templates = {
          default: _.cloneDeep(require("./src/templates/default.json")),
          empty: _.cloneDeep(require("./src/templates/empty.json"))
        };

        // Add the template functions.
        router.formio.template = require("./src/templates/index")(router);

        const swagger = require("./src/util/swagger");
        // Show the swagger for the whole site.
        router.get("/spec.json", function(req, res, next) {
          swagger(req, router, function(spec) {
            res.json(spec);
          });
        });

        // Show the swagger for specific forms.
        router.get("/form/:formId/spec.json", function(req, res, next) {
          swagger(req, router, function(spec) {
            res.json(spec);
          });
        });

        require("./src/middleware/recaptcha")(router);

        // Say we are done.
        deferred.resolve(router.formio);
      });
    });
    /* eslint-enable max-statements */

    return deferred.promise;
  };

  // Return the router.
  return router;
};
