# טפסים api

## התקנה

התקנת מונגו:https://www.mongodb.com/download-center/community

הרצת מונגו: C:\Program Files\MongoDB\Server\3.4\bin>mongod --dbpath "C:\Program Files\MongoDB\Server\3.4\data\db"
בתור אדמין

```bash
npm install
```

## הרצה

:

```bash
mongod
ng serve
```


## דיבג

ניתן לדבג בויזואל סטודיו קוד
צריך להוסיף את הקונפיגורציות המתאימות בקובץ launch.json

```bash
{
      "type": "node",
      "request": "attach",
      "name": "Attach by Process ID",
      "processId": "${command:PickProcess}"
    },
    {
      "type": "node",
      "request": "launch",
      "name": "Launch Program",
      "program": "${workspaceFolder}/app.js"
    }
```

מריצים את הפרוייקט באמצעות ng serve
בוחרים באפשרות של attach to process ID
בוחרים node-> main.jsברשימת האפשרויות

## פרוייקט בסיס

**החלק של הapiה** https://github.com/formio/formio




