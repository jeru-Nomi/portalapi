"use strict";

module.exports = function(router) {
  const hook = require("../util/hook")(router.portal);
  return hook.alter("resources", {
    inbox: require("./InboxResource")(router),
    user: require("./UserResource")(router)
  });
};
