"use strict";

const Resource = require("resourcejs");
const _ = require("lodash");

module.exports = function(router) {
  // Include the hook system.
  const hook = require("../util/hook")(router.portal);
  const util = router.portal.util;

  // @TODO: Fix permission check to use the new roles and permissions system.

  /* eslint-disable new-cap */
  // If the last argument is a function, hook.alter assumes it is a callback function.
  const UserResource = hook.alter("UserResource", Resource, null);

  //   router.portal.mongoose.model("message").find()(function(err, doc) {
  //     const d = doc;
  //   });

  return UserResource(
    router,
    "",
    "user",
    router.portal.mongoose.model("user")
  ).rest();
};
