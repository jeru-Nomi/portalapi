"use strict";

const _ = require("lodash");
const debug = require("debug")("portal:models:inbox");

module.exports = portal => {
  const hook = require("../util/hook")(portal);
  const util = portal.util;
  /* eslint-disable no-useless-escape */

  /* eslint-enable no-useless-escape */

  const model = require("./BaseModel")({
    schema: new portal.mongoose.Schema(
      {
        from: {
          type: String,
          description: "The content for the message.",
          required: false
        },
        to: {
          type: Array,
          description: "The content for the message.",
          required: false
        },
        content: {
          type: String,
          description: "The content for the message.",
          required: false
        },
        subject: {
          type: String,
          description: "The content for the message.",
          required: false
        },
        inboxType: {
          type: String,
          description: "The content for the message.",
          required: false
        },
        msgCount: {
          type: Number,
          description: "The content for the message.",
          required: false
        },
        isMsgNew: {
          type: Boolean,
          description: "The content for the message.",
          required: false
        },
        isImportant: {
          type: Boolean,
          description: "The content for the message.",
          required: false
        },
        statuses: {
          type: Array,
          description: "The content for the message.",
          required: false
        },
        files: {
          type: Array,
          description: "The content for the message.",
          required: false
        },
        forms: {
          type: Array,
          description: "The content for the message.",
          required: false
        },
        isForm: {
          type: Boolean,
          description: "The content for the message.",
          required: false
        },
        isDeleted: {
          type: Boolean,
          description: "The content for the message.",
          required: false
        },
        isStared: {
          type: Boolean,
          description: "The content for the message.",
          required: false
        },
        isMsgForMeeting: {
          type: Boolean,
          description: "The content for the message.",
          required: false
        },
        careProcessStatus: {
          type: Number,
          description: "The content for the message.",
          required: false
        },
        hideInSent: {
          type: Boolean,
          description: "",
          required: false,
          default: undefined
        }

        // title: {
        //   type: String,
        //   description: "The title for the message.",
        //   required: true
        // },
        // content: {
        //   type: String,
        //   description: "The content for the message."
        // }
      },
      { collection: "inbox" }
    )
  });

  //   // Add a partial index for deleted forms.
  //   model.schema.index(
  //     {
  //       deleted: 1
  //     },
  //     {
  //       partialFilterExpression: { deleted: { $eq: null } }
  //     }
  //   );

  // Add machineName to the schema.
  // model.schema.plugin(require("../plugins/machineName")("inbox", portal));

  // Set the default machine name.
  // model.schema.machineName = (document, done) => {
  //   hook.alter("inboxMachineName", document.name, document, done);
  // };

  return model;
};
