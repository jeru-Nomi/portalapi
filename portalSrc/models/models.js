"use strict";

const _ = require("lodash");

module.exports = function(router) {
  // Implement our hook system.
  const hook = require("../util/hook")(router.portal);

  // Define our schemas.
  const models = hook.alter("models", {
    inbox: require("./Inbox")(router.portal),
    user: require("./User")(router.portal)
  });

  const defs = {
    schemas: {},
    models: {},
    specs: {}
  };

  _.each(models, (model, name) => {
    router.portal.mongoose.model(name, model.schema);
    defs.models[name] = model;
    defs.schemas[name] = hook.alter(`${name}Schema`, model.schema, false);
    defs.specs[name] = model.spec;
  });

  // Export the model definitions.
  return defs;
};
