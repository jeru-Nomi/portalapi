"use strict";

const _ = require("lodash");
const debug = require("debug")("portal:models:user");

module.exports = portal => {
  const hook = require("../util/hook")(portal);
  const util = portal.util;
  /* eslint-disable no-useless-escape */

  /* eslint-enable no-useless-escape */

  const model = require("./BaseModel")({
    schema: new portal.mongoose.Schema(
      {
        displayName: {
          type: String,
          description: "The content for the message.",
          required: true
        },
        email: {
          type: String,
          description: "The content for the message.",
          required: true
        },
        needsAuthentication: {
          type: Boolean,
          description: "",
          required: false,
          default: undefined
        },
        pass: {
          type: String,
          description: "The content for the message.",
          required: false
        }
      },
      { collection: "user" }
    )
  });

  //   // Add a partial index for deleted forms.
  //   model.schema.index(
  //     {
  //       deleted: 1
  //     },
  //     {
  //       partialFilterExpression: { deleted: { $eq: null } }
  //     }
  //   );

  // Add machineName to the schema.
  // model.schema.plugin(require("../plugins/machineName")("inbox", portal));

  // Set the default machine name.
  // model.schema.machineName = (document, done) => {
  //   hook.alter("inboxMachineName", document.name, document, done);
  // };

  return model;
};
